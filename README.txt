Code from the OpenCV camera calibration tutorial:
http://docs.opencv.org/doc/tutorials/calib3d/camera_calibration/camera_calibration.html

Instructions
After a successful build:
- copy config.xml, imageless.xml and camera images into your build directory (i.e. where the calibcv executable is)
- for test purposes run ./calibcv config.xml 
  This will open the test images and determine the camera intrinsic, extrinsic and distortion parameters.
- refer to the comments in config.xml for using a live camera